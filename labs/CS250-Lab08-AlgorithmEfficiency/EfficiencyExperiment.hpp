#ifndef _SEARCHER_HPP
#define _SEARCHER_HPP

#include "Timer.hpp"

#include <vector>
using namespace std;

class EfficiencyExperiment
{
    public:
    void SetupVector( int arraySize, bool sorted = false );

    int LinearSearch( int findMe, bool withOutput = false );
    int BinarySearch( int findMe, bool withOutput = false );
    int StudentSearch( int findMe, bool withOutput = false );

    int Fibonacci_Rec( unsigned int n );
    int Fibonacci_Iter( unsigned int n );

    void Run( int type );
    int Size();

    private:
    vector<int> m_array;
    bool m_listSorted;
};

#endif
