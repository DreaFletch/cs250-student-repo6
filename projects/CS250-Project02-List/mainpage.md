# Project Documentation

## Reading documentation

Documentation instructions for the functions are available both in
the source code itself and in this more easily readable webpage.

The webpage version of the docs are available in the **docs** folder.
Open up the **index.html** page to begin reading.

To view the docs, go to **Classes**, then click on the **LinkedList** link.
You can find descriptions for all the functions.

## Reading the test output

When running the tests, it will output some text to the console.
However, to get all the information for the tests (expected output,
comments, etc.) make sure to open up the **test_result.html** file,
located in your project directory.

## About this project

For this project, you will be writing your own Vector class. A Vector
is a type of data structure that wraps a linear, dynamic array. It
takes care of resizing the array automatically so the user doesn't
have to worry about memory management.

You will be working only in the **LinkedList.hpp** file.

### Project files

The project should contain the following code files:

* CS250-Project02-LinkedList/
    * main.cpp
    * Program.hpp
    * Tester.hpp
    * cuTEST/
        * TesterBase.hpp
        * TesterBase.cpp
        * StringUtil.hpp
        * Menu.hpp
    * DataStructure/
        * LinkedList.hpp

## Tackling this project

### You should be implementing one function at a time.

Each function has documentation to help you understand what the
function should do. After implementing **one function**, run the
unit tests to check if your functionality works as intended.

I've ordered the functions in the .hpp file, and the first group of
functions should be easiest to implement.

## Turning in the project

Once finished, turn in your **LinkedList.hpp** file.
